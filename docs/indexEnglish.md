# Iniciação Cientifica : Wetting on Heterogeneous Surfaces

<img src="images\dropsubstrate.jpg" alt="dropsubstrate" width=40% /><img src="images\Cubaud2003.jpg" alt="Cubaud2003" width=50% />

When two materials meet, they form a contact area bounded by a contact line. Wetting is related to the dynamics of contact area between a fluid and a solid [1] (see pictures above) which is governed by intramolecular interaction between the solid, liquid, and gas phases [2]. 

The wetting properties of simple and complex fluids are relevant for a wide range of applications including antibiofouling [[3]](https://en.wikipedia.org/wiki/Biofouling) and hydrophobic coatings [[4]](https://en.wikipedia.org/wiki/Ultrahydrophobicity), medical dressing [[5]](https://en.wikipedia.org/wiki/Dressing_(medical)), water harvester [[6]](https://en.wikipedia.org/wiki/Atmospheric_water_generator), nano-electromechanical systems [[7]](https://en.wikipedia.org/wiki/Nanoelectromechanical_systems). Many phenomena at equilibrium can be explained using a macroscopic approach where the shape of the liquid is understood by a principle of surface minimization. However,  dynamical regimes when the fluid is moving on the substrate are much less understood. The contact line can be subjected to complex dissipation mechanisms in steady state motion and  through instabilities  including stick-slip, avalanches, cavitation, pearling. Many open questions arise concerning the local dissipative processes operating in the dynamical regimes.

## Bolsa 1 : Numerical approach with Surface Evolver

Surface Evolver is a free and open-source interactive program for the study of the shape of liquid surface using a principle of surface minimization. The liquid/air interface is modeled by discrete elements, facets, defined by vertices. The Surface Evolver evolves the surface toward minimal energy by a gradient descent method. Evolver was written at the Geometry Center by Kenneth Brakke [[8]](https://en.wikipedia.org/wiki/Surface_Evolver).

<img src="images\SE_drop.jpg" alt="SE_drop" width=50% />

In this project, the awarded student will learn how to use Surface Evolver to study the shape of drop on a substrate and investigate the influence of heteregoneities.

### Objetivos específicos do estudante

- Training to Surface Evolver;
- Training to collaborative tools Git, GitLab;
- Training to Python for data analysis;
- Developing simulations in simple geometry of a fluid in contact with a solid to understand the software and its capabilities
- Introduce a single heterogeneities in the simulation and measure the shape and energy associated
- Explore more complex situations – Cluster of heterogeneities, lattice of heterogeneities with various symmetries and orientations
- Prepare reports, scientific communcations, poster associated with the present project;
- Participate to scientific events in Brasil

### Resultados específicos do estudante

- Understand the physics of wetting and be up to date with the current litterature
- Have a good master of control of the software Surface Evolver
- Develop simulations to obtain the equilibrium of wetting front in simple and more complex geometry
- Be able to present scientific results in group meetings and scientific events 

## Bolsa 2 : Experimental investigation of capillary phenomena in a Maker space

<img src="images\phenomenamosquito.png" alt="phenomenamosquito" width=37% /><img src="images\PinchedDrop.PNG" alt="PinchedDrop" width=33% />

In this project, the awarded student will develop experimental setups to study some wetting phenomena.  Depending on the access to an experimental lab, he will explore possibilities to study simple capillar effect at home, using a cell phone and a computer to acquire and analyze recorded dynamics. When access is granted, the student will work in the UFBA maker space IHAC-Labi [[9](http://www.ihac.ufba.br/project/ihaclab-i/), [10](https://en.wikipedia.org/wiki/Hackerspace)] and will be trained in digital fabrication (3D printing, laser cutting, etc...).

### Objetivos específicos do estudante

- Training to collaborative tools Git, GitLab;
- Training to digital fabrication (AutoCad,3D printing, Laser cutter) in the HIAC/Lab-i;
- Training to Python for data analysis;
- Designing and building an experimental setup to investigate capillary phenonema
- Prepare reports, scientific communcations, poster associated with the present project;
- Participate to scientific events in Brasil

### Resultados específicos do estudante

- Understand the physics of wetting and be up to date with the current litterature
- Have a good master of control of Digital Fabrication methods
- Know how to design an experiment to answer a scientific question
- Be able to present scientific results in group meetings and scientific events 

#### References

- [1] De Gennes, P. G.; Brochard-Wyart, F.; Quéré, D. (2004) *Capillarity and Wetting Phenomena: Drops, Bubbles, Pearls, Waves*.  Springer-Verlag.
- [2] Israelachvili, J. N. (2011). *Intermolecular and surface forces*. Academic press.
- [3] https://en.wikipedia.org/wiki/Biofouling
- [4] https://en.wikipedia.org/wiki/Ultrahydrophobicity
- [5] https://en.wikipedia.org/wiki/Dressing_(medical)
- [6] https://en.wikipedia.org/wiki/Atmospheric_water_generator
- [7] https://en.wikipedia.org/wiki/Nanoelectromechanical_systems
- [8] https://en.wikipedia.org/wiki/Surface_Evolver
- [9]  https://en.wikipedia.org/wiki/Hackerspace
- [10] http://www.ihac.ufba.br/project/ihaclab-i/